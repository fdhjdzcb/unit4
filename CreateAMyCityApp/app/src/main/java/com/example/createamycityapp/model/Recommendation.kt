package com.example.createamycityapp.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Recommendation(
    val id: Int,
    @StringRes val name: Int,
    @StringRes val category: Int,
    @StringRes val addr: Int,
    @StringRes val descr: Int,
    @DrawableRes val imageResId: Int,
)